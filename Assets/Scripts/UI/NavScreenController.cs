﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavScreenController : BaseScreenController {

    #region Fields / Properties    

    public Button leftButton;
    public Button rightButton;

    #endregion

    #region Events

    public static Action<Direction> NavButtonClickEvent = (Direction direction) => { };

    #endregion

    #region Public Behaviour

    public override void Init () {
        base.Init ();
        leftButton.onClick.AddListener (() => NavButtonClickEvent.Invoke (Direction.Left));
        rightButton.onClick.AddListener (() => NavButtonClickEvent.Invoke (Direction.Right));
    }

    #endregion

}