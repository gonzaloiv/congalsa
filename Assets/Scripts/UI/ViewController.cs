﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewController : MonoBehaviour {

    #region Fields / Properties

    public LoadingScreenController loadingScreenController;
    public ScanScreenController scanScreenController;
    public NavScreenController navScreenController;

    #endregion

    #region Public Behaviour

    public void Init () {
        this.loadingScreenController.Init ();
        this.scanScreenController.Init ();
        this.navScreenController.Init ();
    }

    #endregion

}