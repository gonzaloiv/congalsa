﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppStates {

    public class BaseState : State {

        #region Fields / Properties

        public AppController parent;

        public LoadingScreenController loadingScreenController;
        public ScanScreenController scanScreenController;
        public NavScreenController navScreenController;

        public ItemsController itemsController;

        public App app;

        #endregion

        #region Public Behaviour

        public override void Enter () {
            base.Enter ();
            if (parent == null)
                Init ();
            Debug.LogWarning ("Enter: " + this.GetType ().Name);
        }

        public override void Exit () {
            base.Exit ();
        }

        #endregion

        #region Protected Behaviour

        protected void Init () {
            parent = GetComponent<AppController> ();
            loadingScreenController = parent.viewController.loadingScreenController;
            scanScreenController = parent.viewController.scanScreenController;
            navScreenController = parent.viewController.navScreenController;
            itemsController = parent.sceneController.itemsController;
            app = parent.app;
        }

        #endregion

    }

}