﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppStates  {

    public class LoadingState : BaseState {

        #region Public Behaviour

        public override void Enter () {
            base.Enter ();
            loadingScreenController.Show ();
            parent.ChangeState<ScanState> ();
        }

        public override void Exit () {
            base.Exit ();
            loadingScreenController.Hide ();
        }

        #endregion

    }

}   