﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppStates  {

    public class NavState : BaseState {

        #region Public Behaviour

        public override void Enter () {
            base.Enter ();
            itemsController.Show (app.itemID);
            navScreenController.Show ();
        }

        public override void Exit () {
            base.Exit ();
            navScreenController.Hide ();
        }

        public void OnNavButtonClickEvent (Direction direction) {
            app.itemID += direction == Direction.Right ? 1 : -1;
            if ((int) app.itemID > itemsController.TotalItemCount - 1)
                app.itemID = 0;
            if ((int) app.itemID < 0)
                app.itemID = (ItemID) itemsController.TotalItemCount - 1;
            itemsController.Show (app.itemID);
        }

        #endregion

        #region Protected Behaviour

        protected override void AddListeners () {
            base.AddListeners ();
            NavScreenController.NavButtonClickEvent += OnNavButtonClickEvent;
        }

        protected override void RemoveListeners () {
            base.RemoveListeners ();
            NavScreenController.NavButtonClickEvent -= OnNavButtonClickEvent;
        }

        #endregion

    }

}