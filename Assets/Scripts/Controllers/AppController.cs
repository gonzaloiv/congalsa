﻿using System.Collections;
using System.Collections.Generic;
using AppStates;
using UnityEngine;

// Defines the flow of the app, including data models, systems, scene objects and canvases
// Based on State Machine, has an state for each flow state
public class AppController : StateMachine {

    #region Fields / Properties

    public ViewController viewController;
    public SceneController sceneController;
    public App app;

    #endregion

    #region Mono Behaviour

    private void Awake () {
        viewController.Init ();
        sceneController.Init ();
        app = new App ();
    }

    private void Start () {
        ChangeState<LoadingState> ();
    }

    #endregion

}