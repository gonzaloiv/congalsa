﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyARImageTargetController : ImageTargetController {

    #region Public Behaviour

    public bool hasFoundTarget;

    #endregion

    #region Public Behaviour

    public override void OnTracking (Matrix4x4 pose) {
        base.OnTracking (pose);
    }

    public override void OnLost () {
        base.OnLost ();
    }

    public override void OnFound () {
        base.OnFound ();
        hasFoundTarget = true;
    }

    #endregion

}