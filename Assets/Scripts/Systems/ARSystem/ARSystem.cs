﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Abstracts all the work with AR SDKs, to make easier to interchange them if needed
public class ARSystem : Singleton<ARSystem> {

    #region Fields / Properties

    public EasyARImageTargetController easyARImageTargetController;
    [HideInInspector] public bool isInit = false;

    #endregion

    #region Public Behaviour

    private void Update () {
        if (easyARImageTargetController.hasFoundTarget)
            isInit = true;
    }

    #endregion

}