﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {

    #region Fields / Properties 

    public ItemsController itemsController;

    #endregion

    #region Public Behaviour

    public void Init () {
        itemsController.Init ();
    }

    #endregion

}