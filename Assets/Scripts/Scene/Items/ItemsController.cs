﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsController : BaseMonoBehaviour {

    #region Fields / Properties

    public int TotalItemCount { get { return prefabs.Length; } }
    public ItemController[] prefabs;
    public Transform itemsParent;

    private List<ItemController> items;
    private ItemController item;

    #endregion

    #region Public Behaviour

    public override void Init () {
        items = new List<ItemController> ();
    }

    public void Show (ItemID itemID) {
        if (item != null)
            Hide ();
        item = GetItemByID (itemID);
        item.transform.parent = itemsParent;
        item.Show ();
    }

    public override void Hide () {
        item.transform.parent = transform;
        item.Hide ();
    }

    public void HideAll () {
        for (int i = 0; i < items.Count; i++) {
            items[i].transform.parent = transform;
            items[i].Hide ();
        }
    }

    #endregion

    #region Private Behaviour

    private ItemController GetItemByID (ItemID itemID) {
        ItemController controller = null;
        for (int i = 0; i < items.Count; i++) {
            if (items[i].itemID == itemID)
                controller = items[i];
        }
        if (controller == null)
            controller = InstantiateItemByID (itemID);
        return controller;
    }

    private ItemController InstantiateItemByID (ItemID itemID) {
        for (int i = 0; i < prefabs.Length; i++) {
            if (prefabs[i].itemID == itemID) {
                ItemController controller = Instantiate (prefabs[i], transform).GetComponent<ItemController> ();
                items.Add (controller);
                return controller;
            }
        }
        return null;
    }

    #endregion

}