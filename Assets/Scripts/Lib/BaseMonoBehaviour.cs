﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMonoBehaviour : MonoBehaviour {

    #region Public Behaviour

    public virtual Vector3 Position  { get { return transform.position; } }
    public virtual bool IsActive { get { return gameObject.activeInHierarchy && gameObject.activeSelf; } }
    public string UniqueID {
        get {
            if (uniqueID == null) {
                uniqueID = gameObject.GetComponent<UniqueID> ();
                if (uniqueID == null)
                    uniqueID = gameObject.AddComponent (typeof (UniqueID)) as UniqueID;
            }
            return uniqueID.Get;
        }
    }
    private UniqueID uniqueID;

    // With Delay
    private Coroutine coroutine;
    private WaitForSeconds delay;

    #endregion

    #region Public Behaviour

    public virtual void Init () {
        gameObject.SetActive (false);
    }

    public virtual void Show () {
        gameObject.SetActive (true);
    }

    public virtual void Hide () {
        gameObject.SetActive (false);
    }

    public virtual void SetActive (bool isActive) {
        gameObject.SetActive (isActive);
    }

    public virtual void WithDelay (Action action, float time) {
        delay = new WaitForSeconds (time);
        coroutine = StartCoroutine (WithDelayRoutine (action));
    }

    public virtual void WithCondition (Action action, Func<bool> condition) {
        coroutine = StartCoroutine (WithDelayRoutine (action));
    }

    #endregion

    #region Protected Behaviour

    protected virtual void AddListeners () { }

    protected virtual void RemoveListeners () { }

    #endregion

    #region Private Behaviour

    private IEnumerator WithDelayRoutine (Action action) {
        yield return delay;
        action ();
    }

    private IEnumerator WithConditionRoutine (Action action, Func<bool> condition) {
        while (!condition ())
            yield return null;
        action ();
    }

    #endregion

}