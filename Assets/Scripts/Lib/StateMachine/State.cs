﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State : MonoBehaviour {

    #region Public Behaviour

    public virtual void Enter () {
        AddListeners ();
    }

    public virtual void Exit () {
        RemoveListeners ();
    }

    public virtual void Play () { }

    public virtual void FixedPlay () { }

    #endregion

    #region Protected Behaviour

    protected virtual void AddListeners () { }

    protected virtual void RemoveListeners () { }

    #endregion

}