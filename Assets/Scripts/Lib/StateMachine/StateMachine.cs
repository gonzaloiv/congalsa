﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StateMachine : MonoBehaviour {

    #region Fields / Properties

    protected State currentState;
    protected List<Type> states = new List<Type> ();

    #endregion

    #region Mono Behaviour

    protected virtual void Update () {
        if (currentState)
            currentState.Play ();
    }

    protected virtual void FixedUpdate () {
        if (currentState)
            currentState.FixedPlay ();
    }

    #endregion

    #region Public Behaviour

    public virtual void ChangeState<T> () where T : State {
        PreviousStateExit ();
        currentState = GetState<T> ();
        CurrentStateEnter ();
    }

    public virtual void ChangeState (Type type) {
        PreviousStateExit ();
        currentState = GetState ((Type) type);
        CurrentStateEnter ();
    }

    public virtual bool IsCurrentState<T> () where T : State {
        return currentState == GetState<T> ();
    }

    public virtual void ReturnToPreviousState () {
        if (!states.Any ())
            return;
        PreviousStateExit ();
        currentState = GetState (states[states.Count - 2]);
        CurrentStateEnter ();
    }

    #endregion

    #region Private Behaviour

    protected virtual State GetState<T> () where T : State {
        State state = GetComponent<T> () as State;
        if (state == null)
            state = (gameObject.AddComponent<T> () as State);
        return state;
    }

    protected virtual State GetState (Type type) {
        State state = GetComponent (type) as State;
        if (state == null)
            state = (gameObject.AddComponent (type) as State);
        return state;
    }

    private void PreviousStateExit () {
        if (currentState != null) {
            currentState.Exit ();
            states.Add (currentState.GetType ());
        }
    }

    private void CurrentStateEnter () {
        if (currentState != null)
            currentState.Enter ();
    }

    #endregion

}