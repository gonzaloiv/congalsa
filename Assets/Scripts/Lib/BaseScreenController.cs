﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseScreenController : BaseMonoBehaviour {

    #region Fields / Properties

    public override bool IsActive { get { return canvas.enabled; } }

    private Canvas canvas;

    #endregion

    #region Public Behaviour

    public override void Init () {
        canvas = GetComponent<Canvas> ();
        canvas.enabled = false;
    }

    public override void Show () {
        canvas.enabled = true;
    }

    public override void Hide () {
        canvas.enabled = false;
    }

    public override void SetActive (bool isActive) {
        canvas.enabled = isActive;
    }

    #endregion

}