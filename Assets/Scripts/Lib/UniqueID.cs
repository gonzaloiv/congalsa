﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniqueID : MonoBehaviour {

    #region Fields / Properties

    public string Get {
        get {
            if (id == string.Empty)
                id = System.Guid.NewGuid ().ToString ();
            return id;
        }
    }

    private string id = string.Empty;

    #endregion

}